# YouboraKollusAdapter #

A framework that will collect several video events from the Kollus player and send it to the back end

# Installation #

#### Cloning the repo ####

In shell located in your project folder run

```bash
git clone https://bitbucket.org/npaw/kollus-adapter-ios.git
```

Then copy the "YouboraKollusAdapter" folder to your project and add it to your project.

## How to use ##

## Start plugin and options ##

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

let options = YBOptions()
options.contentResource = "http://example.com"
options.accountCode = "accountCode"
options.adResource = "http://example.com"
options.contentIsLive = NSNumber(value: false)
    
plugin = YBPlugin(options: options)
```

For more information about the options you can check [here](https://documentation.npaw.com/npaw-integration/docs/setting-youbora-options-and-metadata)

### YBKollusAdapter ###

```swift
// Once you have your player and plugin initialized you can set the adapter
plugin.adapter = YBKollusAdapter(player: kollusPlayer)

...

// When the view is going to be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
plugin.fireStop()
plugin.removeAdapter()
```

## Run samples project ##

###### Via the cloned repo ######

Once the repo is cloned navigate to Samples/KollusExample folder and open it. The project should be ready to work.

