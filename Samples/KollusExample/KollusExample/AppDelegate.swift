//
//  AppDelegate.swift
//  KollusExample
//
//  Created by Elisabet Massó on 19/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var kollusStorage: KollusStorage?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if kollusStorage == nil {
            kollusStorage = KollusStorage()
            kollusStorage?.applicationKey = "ea526c88df88fc508fdfe5e0776ca548af6f5b64"
            kollusStorage?.applicationBundleID = "com.etoos.kollus.sample"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            kollusStorage?.applicationExpireDate = dateFormatter.date(from: "2022/03/31")
            kollusStorage?.serverPort = 7781
            
            do {
                try kollusStorage?.start()
            } catch {
                print("Storage Start Err: ", error.localizedDescription)
            }
        }
        let audioSession = AVAudioSession.sharedInstance()
        do {
            // Set the audio session category, mode, and options
            try audioSession.setCategory(.playback, mode: .moviePlayback, options: .mixWithOthers
            )
            try audioSession.setActive(true)
        } catch {
            print("Failed to set audio session category.")
        }
        
        if #available(iOS 13.0, *) {
        } else {
            window = UIWindow(frame: UIScreen.main.bounds)
            let rootViewController = UINavigationController()
            if let menuViewController = MenuViewController.initFromXIB() {
                rootViewController.viewControllers = [menuViewController]
            }
            window?.rootViewController = rootViewController
            window?.makeKeyAndVisible()
        }
        
        return true
    }

}

