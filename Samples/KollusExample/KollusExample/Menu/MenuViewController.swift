//
//  MenuViewController.swift
//  KollusExample
//
//  Created by Elisabet Massó on 19/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit
import YouboraConfigUtils
import YouboraLib

class MenuViewController: UIViewController {
    
    @IBOutlet var resourceTextField: UITextField!
    @IBOutlet var playButton: UIButton!
    
    var plugin: YBPlugin?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        YBLog.setDebugLevel(.debug)

        addSettingsButton()

        resourceTextField.text = "https://v.kr.kollus.com/si?jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYyI6W3sibWNrZXkiOiI3dnNlWmUwViJ9XSwiY3VpZCI6InRlc3RVc2VyISEiLCJleHB0IjoxOTQyMTM2NTQ1fQ.C5uwrzN6tAmesR5cY1rY1AEQounuDA8CwnhV1El94u0&custom_key=37e62a59c28b666877769f95a75f9911978113f5a80a83012b8efd87c1cae64d"
    }

    public static func initFromXIB() -> MenuViewController? {
        return MenuViewController(
            nibName: String(describing: MenuViewController.self),
            bundle: Bundle(for: MenuViewController.self))
    }

}

// MARK: - Settings Section

extension MenuViewController {
    
    func addSettingsButton() {
        guard let navigationController = navigationController else { return }
        addSettingsToNavigation(navigationBar: navigationController.navigationBar)
    }

    func addSettingsToNavigation(navigationBar: UINavigationBar) {
        let settingsButton = UIBarButtonItem(title: "Settings", style: .done, target: self, action: #selector(navigateToSettings))
        navigationBar.topItem?.rightBarButtonItem = settingsButton
    }
    
}

// MARK: - Navigation Section

extension MenuViewController {

    @IBAction func pressToSendOfflineEvents(_ sender: UIButton) {
        let options = YouboraConfigManager.getOptions()
        options.offline = false

        plugin = YBPlugin(options: options)
        
        for _ in 1...3 {
            plugin?.fireOfflineEvents()
        }
    }

    @IBAction func pressButtonToNavigate(_ sender: UIButton) {
        if sender == playButton {
            let playerViewController = PlayerViewController()

            playerViewController.resource = resourceTextField.text

            navigateToViewController(viewController: playerViewController)
            return
        }
    }

    @objc func navigateToSettings() {
        guard let _ = navigationController else {
            navigateToViewController(viewController:
                YouboraConfigViewController.initFromXIB(animatedNavigation: false))
            return
        }

        navigateToViewController(viewController: YouboraConfigViewController.initFromXIB())
    }

    func navigateToViewController(viewController: UIViewController) {
        guard let navigationController = navigationController else {
            present(viewController, animated: true, completion: nil)
            return
        }

        navigationController.pushViewController(viewController, animated: true)
    }

}
