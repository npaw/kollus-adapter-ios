//
//  PlayerViewController.swift
//  KollusExample
//
//  Created by Elisabet Massó on 19/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit
import AVFoundation
import YouboraConfigUtils
import YouboraLib

class PlayerViewController: UIViewController {

    var resource: String?

    var plugin: YBPlugin?
    
    var timer: Timer?
//    var backgroundTime: Double?
    
    var isTouchingSlider = false
    
    var changeItemButton = UIButton(type: .custom)
    var buttonReplay = UIButton(type: .custom)
    var playerContainer = UIView()
    
    private let controlBtn: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "pause")
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleControls), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private let leftTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let currentTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .left
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let videoSlider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = UIColor.red
        slider.maximumTrackTintColor = UIColor.white
        slider.thumbTintColor = UIColor.red
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(handleSliderValueChanged), for: [.touchUpInside, .touchDown])
        slider.isHidden = true
        return slider
    }()
    
    private let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var kollusPlayer: KollusPlayerView! = {
        let player = KollusPlayerView()
        player.translatesAutoresizingMaskIntoConstraints = false
        return player
    }()
    
    private let kollusStorage: KollusStorage? = {
        let appDeletgate = UIApplication.shared.delegate as! AppDelegate
        return appDeletgate.kollusStorage
    }()
    
    private let indicator: UIActivityIndicatorView = {
        let uiIndicator = UIActivityIndicatorView()
        uiIndicator.hidesWhenStopped = true
        uiIndicator.color = .white
        return uiIndicator
    }()
    
    var isPrepared: Bool = false
    var isPlaying: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .moviePlayback, options: .mixWithOthers)
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initializeYoubora()
        releasePlayer()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        becomeFirstResponder()
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        initializePlayer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            timer?.invalidate()
            timer = nil
            plugin?.fireStop()
            plugin?.removeAdapter()
            releasePlayer()
        }
        
    }
    
    private func configureControls() {
        view.backgroundColor = .black
        
        changeItemButton.setTitle("Change Item", for: .normal)
        changeItemButton.addTarget(self, action: #selector(changeItem), for:.touchUpInside)
        changeItemButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(changeItemButton)
        NSLayoutConstraint.activate([
            changeItemButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            changeItemButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            changeItemButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            changeItemButton.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        buttonReplay.setTitle("Replay", for: .normal)
        buttonReplay.addTarget(self, action: #selector(pressToReply), for:.touchUpInside)
        buttonReplay.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonReplay)
        NSLayoutConstraint.activate([
            buttonReplay.topAnchor.constraint(equalTo: changeItemButton.bottomAnchor, constant: 20),
            buttonReplay.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            buttonReplay.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            buttonReplay.heightAnchor.constraint(equalToConstant: 20)
        ])
        buttonReplay.isHidden = true
        
        playerContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(playerContainer)
        NSLayoutConstraint.activate([
            playerContainer.topAnchor.constraint(equalTo: buttonReplay.bottomAnchor, constant: 0),
            playerContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            playerContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            playerContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])

        container.frame = view.bounds
        view.addSubview(container)
        container.topAnchor.constraint(equalTo: playerContainer.topAnchor, constant: 0).isActive = true
        container.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        container.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        container.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 9/16).isActive = true
        
        container.addSubview(indicator)
        container.bringSubviewToFront(indicator)
        indicator.center = container.center
        
        controlBtn.frame = container.bounds
        controlBtn.bounds = controlBtn.frame.insetBy(dx: 20.0, dy: 20.0)
        container.addSubview(controlBtn)
        controlBtn.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 10).isActive = true
        controlBtn.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8).isActive = true
        controlBtn.widthAnchor.constraint(equalToConstant: 15).isActive = true
        controlBtn.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
//        if let contentIsLive = plugin?.options.contentIsLive, contentIsLive.isEqual(to: NSNumber(booleanLiteral: false)) {
            buttonReplay.isHidden = false
            leftTimeLbl.frame = container.bounds
            container.addSubview(leftTimeLbl)
            leftTimeLbl.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -10).isActive = true
            leftTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            leftTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
            leftTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            leftTimeLbl.isHidden = false

            currentTimeLbl.frame = container.bounds
            container.addSubview(currentTimeLbl)
            currentTimeLbl.leftAnchor.constraint(equalTo: controlBtn.rightAnchor, constant: 10).isActive = true
            currentTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            currentTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
            currentTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            currentTimeLbl.isHidden = false

            videoSlider.frame = container.bounds
            container.addSubview(videoSlider)
            videoSlider.rightAnchor.constraint(equalTo: leftTimeLbl.leftAnchor).isActive = true
            videoSlider.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            videoSlider.leftAnchor.constraint(equalTo: currentTimeLbl.rightAnchor).isActive = true
            videoSlider.heightAnchor.constraint(equalToConstant: 30).isActive = true
            videoSlider.isHidden = false
//        }
        
        view.layoutIfNeeded()
    }
    
    @objc
    private func handleControls() {
        if isPrepared && !isPlaying {
            do {
                try kollusPlayer.play()
                controlBtn.setImage(UIImage(named: "pause"), for: .normal)
            } catch {
                print("Player Play: ", error.localizedDescription)
            }
        } else if isPrepared && isPlaying {
            do {
                try kollusPlayer.pause()
                controlBtn.setImage(UIImage(named: "play"), for: .normal)
            } catch {
                print("Player pause: ", error.localizedDescription)
            }
        } else {
            do {
                try kollusPlayer.play()
                controlBtn.setImage(UIImage(named: "pause"), for: .normal)
            } catch {
                print("Player Play: ", error.localizedDescription)
            }
        }
    }
    
    @objc
    private func handleSliderValueChanged(sender: UISlider) {
        if let totalSeconds = kollusPlayer?.content?.duration {
            let value = Double(videoSlider.value) * totalSeconds
            kollusPlayer.currentPlaybackTime = value
            isTouchingSlider = !isTouchingSlider
        }
    }
    
    @objc
    func handleTime() {
        if let currentTime = kollusPlayer?.currentPlaybackTime, let duration = kollusPlayer?.content?.duration {
            currentTimeLbl.text = "\(getTextTimeFrom(time: currentTime))"
            leftTimeLbl.text = "-\(getTextTimeFrom(time: duration - currentTime))"
            if !isTouchingSlider {
                videoSlider.value = Float(currentTime / duration)
            }
        }
    }
    
    @objc
    func changeItem() {
        guard let newResource = ["https://v.kr.kollus.com/si?jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYyI6W3sibWNrZXkiOiI3dnNlWmUwViJ9XSwiY3VpZCI6InRlc3RVc2VyISEiLCJleHB0IjoxOTQyMTM2NTQ1fQ.C5uwrzN6tAmesR5cY1rY1AEQounuDA8CwnhV1El94u0&custom_key=37e62a59c28b666877769f95a75f9911978113f5a80a83012b8efd87c1cae64d"].randomElement() else {
            return
        }
        resource = newResource
        plugin?.options.contentIsLive = NSNumber(false)
        
        // Change content in player
        releasePlayer()
        
        configureControls()
        kollusPlayer.contentURL = resource
        do {
            indicator.startAnimating()
            try kollusPlayer.prepareToPlay(withMode: .PlayerTypeNative)
        } catch {
            print("An error occurred while preparing the content: ", error.localizedDescription)
        }
        
        configureControls()
    }
    
    @objc
    func pressToReply() {
        if kollusPlayer.currentPlaybackTime > 0 {
            kollusPlayer.currentPlaybackTime = 0
        } else {
            releasePlayer()
            do {
                indicator.startAnimating()
                try kollusPlayer.prepareToPlay(withMode: .PlayerTypeNative)
            } catch {
                print("An error occurred while preparing the content: ", error.localizedDescription)
            }
        }
    }
    
    private func getTextTimeFrom(time totalSeconds: Double) -> String {
        if !totalSeconds.isNaN && !totalSeconds.isInfinite {
            let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
            let secondsText = String(format: "%02d", seconds)
            let minutesText = String(format: "%02d", Int(totalSeconds / 60))
            return "\(minutesText):\(secondsText)"
        }
        return ""
    }

}

extension PlayerViewController {
    
    private func addNotificationObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(willEnterForeground(_:)),
            name: UIApplication.willEnterForegroundNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didEnterBackground(_:)),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil
        )
    }
    
    @objc private func willEnterForeground(_ notification: Notification) {
        handleControls()
    }
    
    @objc private func didEnterBackground(_ notification: Notification) {
        
    }
    
}

// MARK: - Video

extension PlayerViewController {
    
    private func initializeYoubora() {
        let options = YouboraConfigManager.getOptions()
        plugin = YBPlugin(options: options)
    }
    
    func loadPlayer(with resource: String) {
        kollusPlayer.removeFromSuperview()
        configureControls()
        
        kollusPlayer = {
            let player = KollusPlayerView()
            player.translatesAutoresizingMaskIntoConstraints = false
            player.frame = playerContainer.bounds
            player.storage = kollusStorage
            player.contentURL = resource
            player.debug = true
            return player
        }()
        playerContainer.addSubview(kollusPlayer)
        kollusPlayer.delegate = self
        kollusPlayer.drmDelegate = self
        kollusPlayer.lmsDelegate = self
        kollusPlayer.bookmarkDelegate = self
        
        do {
            indicator.startAnimating()
            try kollusPlayer.prepareToPlay(withMode: .PlayerTypeNative)
        } catch {
            print("An error occurred while preparing the content: ", error.localizedDescription)
        }
        
    }
    
    func initializePlayer() {
        // Make a Shaka Player with its corresponding view.
        guard let resource = resource else {
            print("Error creating player")
            return
        }
        
        // Load and play an asset.
        loadPlayer(with: resource)
        
        plugin?.adapter = YBKollusAdapter(player: kollusPlayer)
        plugin?.fireInit() // Should send fireInit as the events only start when player starts to play
        
        addNotificationObserver()
    }
    
    func releasePlayer() {
        if kollusPlayer != nil {
            do {
                if kollusPlayer.storage == nil {
                    kollusPlayer.storage = kollusStorage
                }
                try kollusPlayer.stop()
                self.kollusPlayer.removeFromSuperview()
            } catch {
                print("Player Release Error: ", error.localizedDescription)
            }
        }
    }
    
}

extension PlayerViewController: KollusPlayerDelegate, KollusPlayerDRMDelegate, KollusPlayerLMSDelegate, KollusPlayerBookmarkDelegate {
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, prepareToPlayWithError error: Error!) {
        indicator.stopAnimating()
        if error != nil {
            print("Prepare Error: ", error.debugDescription)
        } else {
            isPrepared = true
            isPlaying = false
            handleControls()
        }
    }


    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, play userInteraction: Bool, error: Error!) {
        isPlaying = true
        controlBtn.isHidden = false
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(handleTime), userInfo: nil, repeats: true)
        }
    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, pause userInteraction: Bool, error: Error!) {
        let state = UIApplication.shared.applicationState
        if (state == .background || state == .inactive) && !userInteraction {
            handleControls()
        } else {
            controlBtn.setImage(UIImage(named: "resume"), for: .normal)
            if timer != nil {
                timer?.invalidate()
                timer = nil
            }
        }
        isPlaying = false
    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, buffering: Bool, error: Error!) {
        if buffering {
            indicator.startAnimating()
        }
    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, stop userInteraction: Bool, error: Error!) {
        isPlaying = false
        isPrepared = false
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        releasePlayer()
    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, position: TimeInterval, error: Error!) {
        print("Progress Event Raise", position)
    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, scroll distance: CGPoint, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, zoom recognizer: UIPinchGestureRecognizer!, error: NSErrorPointer) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, naturalSize: CGSize) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playerContentMode: KollusPlayerContentMode, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playerContentFrame contentFrame: CGRect, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playbackRate: Float, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, repeat: Bool, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, enabledOutput: Bool, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, unknownError error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, framerate: Int32) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, lockedPlayer playerType: KollusPlayerType) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, charset: UnsafeMutablePointer<Int8>!, caption: UnsafeMutablePointer<Int8>!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, thumbnail isThumbnail: Bool, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, mck: String!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, request: [AnyHashable: Any]!, json: [AnyHashable: Any]!, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, json: [AnyHashable: Any]!, error: Error!) {

    }

    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, bookmark bookmarks: [Any]!, enabled: Bool, error: Error!) {

    }
}
