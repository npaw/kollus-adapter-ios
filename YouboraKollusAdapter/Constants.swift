//
//  Constants.swift
//  YouboraKollusAdapter
//
//  Created by Elisabet Massó on 11/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation

final class Constants {
    
    static func getAdapterName() -> String {
        return "\(getName())-\(getPlatform())"
    }
    
    static func getAdapterVersion() -> String {
        return "\(getVersion())-\(getAdapterName())"
    }
    
    static func getAdsAdapterName() -> String {
        return "\(getName())-Ads-\(getPlatform())"
    }
    
    static func getAdsAdapterVersion() -> String {
        return "\(getVersion())-\(getAdsAdapterName())"
    }
    
    static func getName() -> String {
        return "Kollus"
    }
    
    static func getPlatform() -> String {
        #if os(tvOS)
        return "tvOS"
        #else
        return "iOS"
        #endif
    }
    
    static func getVersion() -> String {
        return "6.6.0"
    }
    
}
