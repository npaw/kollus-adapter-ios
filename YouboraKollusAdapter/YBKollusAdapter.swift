//
//  YBKollusAdapter.swift
//  YouboraKollusAdapter
//
//  Created by Elisabet Massó on 11/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import YouboraLib

public class YBKollusAdapter: YBPlayerAdapter<AnyObject> {
    
    private var delegate: KollusPlayerDelegate?
    private var drmDelegate: KollusPlayerDRMDelegate?
    private var lmsDelegate: KollusPlayerLMSDelegate?
    private var bookmarkDelegate: KollusPlayerBookmarkDelegate?
    
    private var joinTimer: Timer?

    private override init() {
        super.init()
    }
    
    public init(player: KollusPlayerView) {
        super.init(player: player)
    }
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? KollusPlayerView {
            delegate = player.delegate
            drmDelegate = player.drmDelegate
            lmsDelegate = player.lmsDelegate
            bookmarkDelegate = player.bookmarkDelegate
            player.delegate = self
            player.drmDelegate = self
            player.lmsDelegate = self
            player.bookmarkDelegate = self
        }
        monitorPlayhead(withBuffers: true, seeks: true, andInterval: 800)
    }
    
    public override func unregisterListeners() {
        if let player = player as? KollusPlayerView {
            player.delegate = delegate
            player.drmDelegate = drmDelegate
            player.lmsDelegate = lmsDelegate
            player.bookmarkDelegate = bookmarkDelegate
        }
        delegate = nil
        drmDelegate = nil
        lmsDelegate = nil
        bookmarkDelegate = nil
        monitor?.stop()
        super.unregisterListeners()
    }
    
    // MARK: - Getters
    
    public override func getDuration() -> NSNumber? {
        guard let player = player as? KollusPlayerView, let duration = player.content?.duration else { return super.getDuration() }
        return NSNumber(value: duration)
    }
    
    public override func getIsLive() -> NSValue? {
        guard let player = player as? KollusPlayerView else { return super.getDuration() }
//        return NSNumber(booleanLiteral: player.isLive ? true : (getDuration() != nil))
        return NSNumber(value: player.isLive)
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let player = player as? KollusPlayerView else { return super.getPlayhead() }
        return NSNumber(value: player.currentPlaybackTime)
    }
    
    public override func getPlayrate() -> NSNumber {
        guard let player = player as? KollusPlayerView else { return super.getPlayrate() }
        if flags.paused {
            return 0
        }
        return NSNumber(value: player.currentPlaybackRate)
    }
    
    public override func getResource() -> String? {
        guard let player = player as? KollusPlayerView else { return super.getResource() }
        return player.contentURL
    }
    
    public override func getTitle() -> String? {
        guard let player = player as? KollusPlayerView else { return super.getTitle() }
        return player.content?.title
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdapterVersion()
    }
    
}

extension YBKollusAdapter: KollusPlayerDelegate {
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, prepareToPlayWithError error: Error!) {
        checkIfError(error)
        if flags.joined {
            fireStop()
        }
        delegate?.kollusPlayerView(kollusPlayerView, prepareToPlayWithError: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, play userInteraction: Bool, error: Error!) {
        checkIfError(error)
        if !flags.started {
            joinTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(joinTimeUpdate), userInfo: nil, repeats: true)
        }
        fireResume()
        fireStart()
        delegate?.kollusPlayerView(kollusPlayerView, play: userInteraction, error: error)
    }
    
    @objc private func joinTimeUpdate() {
        guard let player = player as? KollusPlayerView else {
            joinTimer?.invalidate()
            joinTimer = nil
            return
        }
        if player.currentPlaybackTime > 0 {
            fireJoin()
            joinTimer?.invalidate()
            joinTimer = nil
        }
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, pause userInteraction: Bool, error: Error!) {
        checkIfError(error)
        firePause()
        delegate?.kollusPlayerView(kollusPlayerView, pause: userInteraction, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, buffering: Bool, error: Error!) {
        checkIfError(error)
        delegate?.kollusPlayerView(kollusPlayerView, buffering: buffering, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, stop userInteraction: Bool, error: Error!) {
        checkIfError(error)
        fireStop()
        delegate?.kollusPlayerView(kollusPlayerView, stop: userInteraction, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, position: TimeInterval, error: Error!) {
        checkIfError(error)
        delegate?.kollusPlayerView(kollusPlayerView, position: position, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, scroll distance: CGPoint, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, scroll: distance, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, zoom recognizer: UIPinchGestureRecognizer!, error: NSErrorPointer) {
        delegate?.kollusPlayerView(kollusPlayerView, zoom: recognizer, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, naturalSize: CGSize) {
        delegate?.kollusPlayerView(kollusPlayerView, naturalSize: naturalSize)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playerContentMode: KollusPlayerContentMode, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, playerContentMode: playerContentMode, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playerContentFrame contentFrame: CGRect, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, playerContentFrame: contentFrame, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playbackRate: Float, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, playbackRate: playbackRate, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, repeat: Bool, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, repeat: `repeat`, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, enabledOutput: Bool, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, enabledOutput: enabledOutput, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, unknownError error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, unknownError: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, framerate: Int32) {
        delegate?.kollusPlayerView(kollusPlayerView, framerate: framerate)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, lockedPlayer playerType: KollusPlayerType) {
        delegate?.kollusPlayerView(kollusPlayerView, lockedPlayer: playerType)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, charset: UnsafeMutablePointer<CChar>!, caption: UnsafeMutablePointer<CChar>!) {
        delegate?.kollusPlayerView(kollusPlayerView, charset: charset, caption: caption)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, thumbnail isThumbnail: Bool, error: Error!) {
        delegate?.kollusPlayerView(kollusPlayerView, thumbnail: isThumbnail, error: error)
    }
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, mck: String!) {
        delegate?.kollusPlayerView(kollusPlayerView, mck: mck)
    }
    
    func checkIfError(_ error: Error?) {
        if let error = error as NSError? {
            fireError(withMessage: error.localizedDescription, code: "\(error.code)", andMetadata: nil)
        }
    }
    
}

extension YBKollusAdapter: KollusPlayerDRMDelegate {
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, request: [AnyHashable : Any]!, json: [AnyHashable : Any]!, error: Error!) {
        drmDelegate?.kollusPlayerView(kollusPlayerView, request: request, json: json, error: error)
    }
    
}

extension YBKollusAdapter: KollusPlayerLMSDelegate {
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, json: [AnyHashable : Any]!, error: Error!) {
        lmsDelegate?.kollusPlayerView(kollusPlayerView, json: json, error: error)
    }
    
}

extension YBKollusAdapter: KollusPlayerBookmarkDelegate {
    
    public func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, bookmark bookmarks: [Any]!, enabled: Bool, error: Error!) {
        bookmarkDelegate?.kollusPlayerView(kollusPlayerView, bookmark: bookmarks, enabled: enabled, error: error)
    }
    
}
